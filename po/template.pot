# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-04-20 12:45+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/Accounts.qml:78
msgid "Choose an ownCloud Account"
msgstr ""

#: ../src/Accounts.qml:99
msgid ""
"No ownCloud accounts available. Tap on the button below to add an account."
msgstr ""

#: ../src/Accounts.qml:104
msgid "Add a new account"
msgstr ""

#: ../src/Accounts.qml:109
msgid "Cancel"
msgstr ""

#: ../src/BookmarkDelegate.qml:90
#, qt-format
msgid "%1%"
msgstr ""

#: ../src/BookmarkInfo.qml:30
msgid "No title"
msgstr ""

#: ../src/BookmarkInfo.qml:38
msgid "No description"
msgstr ""

#: ../src/BookmarkInfo.qml:53
msgid "Tags:"
msgstr ""

#: ../src/BookmarkInfo.qml:53
msgid "No tags"
msgstr ""

#: ../src/BookmarkInfo.qml:123
msgid "..."
msgstr ""

#: ../src/EditPage.qml:13
msgid "Edit metadata"
msgstr ""

#: ../src/EditPage.qml:33
msgid "Title"
msgstr ""

#: ../src/EditPage.qml:38
msgid "Enter a title"
msgstr ""

#: ../src/EditPage.qml:49
msgid "Description"
msgstr ""

#: ../src/EditPage.qml:56
msgid "Enter a description"
msgstr ""

#: ../src/EditPage.qml:66
msgid "Tags"
msgstr ""

#: ../src/EditPage.qml:113
msgid "Add tag"
msgstr ""

#. TRANSLATORS: This is a key on the OSK, keep it short.
#: ../src/EditPage.qml:116
msgctxt "OSK"
msgid "Add tag"
msgstr ""

#: ../src/ImportPrompt.qml:33
msgid "No bookmarks."
msgstr ""

#: ../src/Main.qml:88
msgid "ownCloud bookmarks"
msgstr ""

#: ../common-config.pri:6
msgid "ownCLoud bookmarks"
msgstr ""

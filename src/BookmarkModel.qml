/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0

ListModel {
    id: root

    property string host: ""

    onHostChanged: console.log("Host changed to " + host)

    function login(username, password) {
        root.clear()
        console.log("Logging in as " + username)
        var http = new XMLHttpRequest()
        var url = host +"/index.php/apps/bookmarks/public/rest/v1/bookmark?user=" + username + "&password=" + password;
        http.open("GET", url, true);
        http.onreadystatechange = function() {
            if (http.readyState === 4){
                if (http.status == 200) {
                    console.log("ok")
                    console.log("response text: " + http.responseText)
                    var response = JSON.parse(http.responseText)
                    parseResponse(response)
                    return true
                } else {
                    console.log("error: " + http.status)
                    return false
                }
            }
        };

        http.send(null);
    }

    function parseResponse(response) {
        for (var i = 0; i < response.length; i++) {
            var item = response[i]
            console.log("Appending " + JSON.stringify(item))
            root.append({
                "url": item.url,
                "title": item.title,
                "description": "",
                "tags": [],
            })
        }
    }
}

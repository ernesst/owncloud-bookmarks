include(../common-config.pri)

TEMPLATE = app
TARGET = app

QT += \
    qml \
    quick

CONFIG += \
    c++11

SOURCES += \
    main.cpp

HEADERS += \


RESOURCES += \
    app.qrc \
    $${TOP_SRC_DIR}/data/icons/icons.qrc

DEFINES += \
    APPLICATION_NAME=\\\"$${APPLICATION_NAME}\\\"

target.path = $${INSTALL_PREFIX}/bin
INSTALLS+=target

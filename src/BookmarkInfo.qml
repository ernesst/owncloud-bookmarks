/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root

    property string title: ""
    property string description: ""
    property alias tags: tagRepeater.model

    Label {
        id: titleLabel
        anchors { left: parent.left; right: parent.right; top: parent.top }
        text: title ? title : i18n.tr("No title")
        fontSize: "large"
        elide: Text.ElideRight
    }

    Label {
        id: descriptionLabel
        anchors { left: parent.left; right: parent.right; top: titleLabel.bottom }
        text: description ? description : i18n.tr("No description")
        elide: Text.ElideRight
        wrapMode: Text.WordWrap
        maximumLineCount: 2
    }

    Flow {
        id: flow
        anchors {
            left: parent.left; right: parent.right
            top: descriptionLabel.bottom; bottom: parent.bottom
        }
        spacing: units.gu(0.5)
        Label {
            height: contentHeight + units.gu(1)
            text: tags.length > 0 ? i18n.tr("Tags:") : i18n.tr("No tags")
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }
        Repeater {
            id: tagRepeater
            model: tags
            Rectangle {
                width: contents.width + units.gu(1)
                height: contents.height + units.gu(1)
                color: "white"
                opacity: (y + height < flow.height) ? 1.0 : 0.0
                radius: units.gu(1)

                Item {
                    id: contents
                    anchors.centerIn: parent
                    width: childrenRect.width
                    height: childrenRect.height
                    Label {
                        id: tagLabel
                        width: Math.min(flow.width, implicitWidth)
                        verticalAlignment: Text.AlignVCenter
                        text: modelData
                        fontSize: "small"
                        elide: Text.ElideRight
                    }
                }
            }
        }

        function lastVisibleItem() {
            var item = null
            for (var i = 0; i < tagRepeater.count; i++) {
                if (tagRepeater.itemAt(i).opacity > 0) {
                    item = tagRepeater.itemAt(i)
                }
            }
            return item
        }
    }

    Item {
        anchors.fill: flow
        Rectangle {
            property Item lastVisibleItem: flow.lastVisibleItem()
            y: lastVisibleItem ? lastVisibleItem.y : 0
            x: lastVisibleItem ? Math.min(lastVisibleItem.x + lastVisibleItem.width + flow.spacing, parent.width - width) : 0
            width: contents.width + units.gu(1)
            height: contents.height + units.gu(1)
            visible: false
            color: "white"
            onLastVisibleItemChanged: {
                if (lastVisibleItem != null &&
                    lastVisibleItem == tagRepeater.itemAt(tagRepeater.count - 1)) {
                    visible = false
                } else {
                    visible = true
                }
            }
            radius: units.gu(1)

            Item {
                id: contents
                anchors.centerIn: parent
                width: childrenRect.width
                height: childrenRect.height
                Label {
                    id: tagLabel
                    verticalAlignment: Text.AlignVCenter
                    text: i18n.tr("...")
                    fontSize: "small"
                }
            }
        }
    }
}

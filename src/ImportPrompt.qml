/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.3

Column {
    id: root

    signal importRequested

    anchors.centerIn: parent
    width: Math.min(units.gu(60), parent.width)
    spacing: units.gu(2)

    Label {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: units.gu(1)
        text: i18n.tr("No bookmarks.")
        wrapMode: Text.Wrap
    }
}
